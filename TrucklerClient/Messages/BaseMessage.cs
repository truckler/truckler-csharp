﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public abstract class BaseMessage
    {
        public EMessageType EMessageType { get; protected set; }
        public int MessageType { get; protected set; }
        public int MessageSize { get; protected set; }
        public readonly byte[] Raw;

        public bool HasError { get; protected set; }
        public Exception? Exception { get; protected set; }

        public BaseMessage(IEnumerable<byte> data, bool discardHeader = true)
        {
            HasError = true;
            Exception = new NullReferenceException("Not Initialized");
            try
            {
                Raw = data.ToArray();
                MessageType = BitConverter.ToInt32(Raw, 0);
                MessageSize = BitConverter.ToInt32(Raw, sizeof(int));
                EMessageType = Messages.MessageType.IntToMessageType(MessageType);
                if (discardHeader)
                {
                    Raw = data.Skip(sizeof(int) * 4).ToArray();
                }
                HasError = false;
                Exception = null;
            }
            catch(Exception ex)
            {
                HasError = true;
                Exception = ex;
                Raw = data?.ToArray() ?? new byte[] { };
                MessageType = Messages.MessageType.Unknown;
                EMessageType = Messages.EMessageType.Unknown;
            }
        }

    }
}
