﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientSubscribeEvents : BaseMessage
    {
        internal ClientSubscribeEvents(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientSubscribeEvents Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Subscribe_Events));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientSubscribeEvents(data);
        }
    }
}
