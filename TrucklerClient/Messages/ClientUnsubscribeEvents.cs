﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientUnsubscribeEvents : BaseMessage
    {
        internal ClientUnsubscribeEvents(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientUnsubscribeEvents Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Unsubscribe_Events));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientUnsubscribeEvents(data);
        }
    }
}
