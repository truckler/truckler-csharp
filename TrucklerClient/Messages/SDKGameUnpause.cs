﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class SDKGameUnpause : BaseMessage
    {
        internal SDKGameUnpause(IEnumerable<byte> data) : base(data)
        {
            if (HasError)
            {
                return;
            }
            Exception = null;
        }
    }
}
