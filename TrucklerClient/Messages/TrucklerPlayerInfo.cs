﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class TrucklerPlayerInfo : BaseMessage
    {
        [JsonProperty("steamUser")]
        public readonly Objects.Truckler.SteamUser SteamUser;
        [JsonProperty("discordUser")]
        public readonly Objects.Truckler.DiscordUser DiscordUser;
        [JsonProperty("game")]
        public readonly Objects.Truckler.Game Game;
        [JsonProperty("plugin")]
        public readonly Objects.Truckler.Plugin Plugin;
        internal TrucklerPlayerInfo(IEnumerable<byte> data) : base(data)
        {
            //initialize properties
            SteamUser = new Objects.Truckler.SteamUser();
            DiscordUser = new Objects.Truckler.DiscordUser();
            Game = new Objects.Truckler.Game();
            Plugin = new Objects.Truckler.Plugin();
            //---------------------
            if (HasError)
            {
                return;
            }
            try
            {
                //process message
                new JsonSerializer().Populate(new JsonTextReader(new StringReader(System.Text.UTF8Encoding.UTF8.GetString(Raw))), this);
                //---------------
                HasError = false;
                Exception = null;
            }
            catch (Exception ex)
            {
                HasError = true;
                Exception = ex;
            }
        }
    }
}
