﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ServerAccept : BaseMessage
    {
        public int ClientId { get; internal set; }
        public bool HasClientId { get; internal set; }


        internal ServerAccept(IEnumerable<byte> data):base(data,false)
        {

            ClientId = -1;
            HasClientId = false;
            
            if(HasError)
            {
                return;
            }
            try
            {
                if (Raw.Length >= sizeof(int))
                {
                    int curPos = sizeof(int)*2;
                    ClientId = BitConverter.ToInt32(Raw, curPos);
                    HasClientId = true;
                }
            }
            catch(Exception ex)
            {
                HasError = true;
                Exception = ex;
            }
        }
    }
}
