﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientRequestHShifterConfiguration : BaseMessage
    {
        
        internal ClientRequestHShifterConfiguration(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientRequestHShifterConfiguration Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Request_HShifter_Configuration));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientRequestHShifterConfiguration(data);
        }
    }
}
