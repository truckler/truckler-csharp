﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class SDKGameplayEventJobCancelled : BaseMessage
    {
        [JsonProperty("gameplayEvent")]
        public readonly Objects.GameplayEvents.JobCancelled GameplayEvent;
        
        [JsonProperty("truckler")]
        public readonly Objects.Truckler.JobCancelled TrucklerDetails;
        internal SDKGameplayEventJobCancelled(IEnumerable<byte> data) : base(data)
        {
            //initialize properties
            GameplayEvent = new Objects.GameplayEvents.JobCancelled();
            TrucklerDetails = new Objects.Truckler.JobCancelled();
            //---------------------
            if (HasError)
            {
                return;
            }
            try
            {
                //process message
                new JsonSerializer().Populate(new JsonTextReader(new StringReader(System.Text.UTF8Encoding.UTF8.GetString(Raw))), this);
                //---------------
                HasError = false;
                Exception = null;
            }
            catch (Exception ex)
            {
                HasError = true;
                Exception = ex;
            }
        }

    }
}
