﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class TrucklerTelemetryFrameData : BaseMessage
    {
        [JsonProperty("game")]
        public readonly Objects.Truckler.FrameData.Game Game;
        [JsonProperty("truck")]
        public readonly Objects.Truckler.FrameData.Truck Truck;
        [JsonProperty("trailers")]
        public readonly Objects.Truckler.FrameData.Trailer[] Trailers;
        [JsonProperty("navigation")]
        public readonly Objects.Truckler.FrameData.Navigation Navigation;
        [JsonProperty("job")]
        public readonly Objects.Truckler.FrameData.Job Job;
        internal TrucklerTelemetryFrameData(IEnumerable<byte> data) : base(data)
        {
            //initialize properties
            Game = new Objects.Truckler.FrameData.Game();
            Truck = new Objects.Truckler.FrameData.Truck();
            Trailers = new Objects.Truckler.FrameData.Trailer[0];
            Navigation = new Objects.Truckler.FrameData.Navigation();
            Job = new Objects.Truckler.FrameData.Job();
            //---------------------
            if (HasError)
            {
                return;
            }
            try
            {
                //process message
                new JsonSerializer().Populate(new JsonTextReader(new StringReader(System.Text.UTF8Encoding.UTF8.GetString(Raw))), this);
                //---------------
                HasError = false;
                Exception = null;
            }
            catch (Exception ex)
            {
                HasError = true;
                Exception = ex;
            }
        }

    }
}
