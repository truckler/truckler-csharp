﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{   
    public enum EMessageType
    {
		ServerAccept,
		ServerDeny,
		ServerShutdown,

		Client_Subscribe_Events,
		Client_Unsubscribe_Events,
		Client_Request_Telemetry,
		Client_Request_PlayerInfo,
		Client_Request_Substance_Configuration,
		Client_Request_Truck_Configuration,
		Client_Request_Job_Configuration,
		Client_Request_Trailer_Configuration,
		Client_Request_ShifterType_Configuration,
		Client_Request_HShifter_Configuration,

		SDK_Configuration_Truck,
		SDK_Configuration_Job,
		SDK_Configuration_Trailer,
		SDK_Configuration_Substances,
		SDK_Conifguration_ShifterType,
		SDK_Configuration_HShifter,
		SDK_Gameplay_Event_Player_Use_Train,
		SDK_Gameplay_Event_Player_Use_Ferry,
		SDK_Gameplay_Event_Player_Tollgate_Paid,
		SDK_Gameplay_Event_Player_Fined,
		SDK_Gameplay_Event_Job_Delivered,
		SDK_Gameplay_Event_Job_Cancelled,
		SDK_Game_Pause,
		SDK_Game_Unpause,


		Truckler_Telemetry_Frame_Data,
		Truckler_Gameplay_Event,
		Truckler_Player_Info,

		Unknown=-1
	}
	public class MessageType
    {
		public static readonly int Unknown = -1;
		public static readonly int ServerAccept = 0;
		public static readonly int ServerDeny = 1;
		public static readonly int ServerShutdown = 2;
		public static readonly int Client_Subscribe_Events = 3;
		public static readonly int Client_Unsubscribe_Events = 4;
		public static readonly int Client_Request_Telemetry = 5;
		public static readonly int Client_Request_PlayerInfo = 6;
		public static readonly int Client_Request_Substance_Configuration = 7;
		public static readonly int Client_Request_Truck_Configuration = 8;
		public static readonly int Client_Request_Job_Configuration = 9;
		public static readonly int Client_Request_Trailer_Configuration = 10;
		public static readonly int Client_Request_ShifterType_Configuration = 11;
		public static readonly int Client_Request_HShifter_Configuration = 12;
		public static readonly int SDK_Configuration_Truck = 13;
		public static readonly int SDK_Configuration_Job = 14;
		public static readonly int SDK_Configuration_Trailer = 15;
		public static readonly int SDK_Configuration_Substances = 16;
		public static readonly int SDK_Conifguration_ShifterType = 17;
		public static readonly int SDK_Configuration_Hshifter = 18;
		public static readonly int SDK_Gameplay_Event_Player_Use_Train = 19;
		public static readonly int SDK_Gameplay_Event_Player_Use_Ferry = 20;
		public static readonly int SDK_Gameplay_Event_Player_Tollgate_Paid = 21;
		public static readonly int SDK_Gameplay_Event_Player_Fined = 22;
		public static readonly int SDK_Gameplay_Event_Job_Delivered = 23;
		public static readonly int SDK_Gameplay_Event_Job_Cancelled = 24;
		public static readonly int SDK_Game_Pause = 25;
		public static readonly int SDK_Game_Unpause = 26;
		public static readonly int Truckler_Telemetry_Frame_Data = 27;
		public static readonly int Truckler_Gameplay_Event = 28;
		public static readonly int Truckler_Player_Info = 29;
		public static int MessageTypeToInt(EMessageType val)
        {
			switch(val)
            {
				case EMessageType.ServerAccept:
					return ServerAccept;
				case EMessageType.ServerDeny:
					return ServerDeny;
				case EMessageType.ServerShutdown:
					return ServerShutdown;
				case EMessageType.Client_Subscribe_Events:
					return Client_Subscribe_Events;
				case EMessageType.Client_Unsubscribe_Events:
					return Client_Unsubscribe_Events;
				case EMessageType.Client_Request_Telemetry:
					return Client_Request_Telemetry;
				case EMessageType.Client_Request_PlayerInfo:
					return Client_Request_PlayerInfo;
				case EMessageType.Client_Request_HShifter_Configuration:
					return Client_Request_HShifter_Configuration;
				case EMessageType.Client_Request_Job_Configuration:
					return Client_Request_Job_Configuration;
				case EMessageType.Client_Request_ShifterType_Configuration:
					return Client_Request_ShifterType_Configuration;
				case EMessageType.Client_Request_Substance_Configuration:
					return Client_Request_Substance_Configuration;
				case EMessageType.Client_Request_Trailer_Configuration:
					return Client_Request_Trailer_Configuration;
				case EMessageType.Client_Request_Truck_Configuration:
					return Client_Request_Truck_Configuration;
				case EMessageType.SDK_Configuration_Truck:
					return SDK_Configuration_Truck;
				case EMessageType.SDK_Configuration_Job:
					return SDK_Configuration_Job;
				case EMessageType.SDK_Configuration_Trailer:
					return SDK_Configuration_Trailer;
				case EMessageType.SDK_Configuration_Substances:
					return SDK_Configuration_Substances;
				case EMessageType.SDK_Conifguration_ShifterType:
					return SDK_Conifguration_ShifterType;
				case EMessageType.SDK_Configuration_HShifter:
					return SDK_Configuration_Hshifter;
				case EMessageType.SDK_Gameplay_Event_Player_Use_Train:
					return SDK_Gameplay_Event_Player_Use_Train;
				case EMessageType.SDK_Gameplay_Event_Player_Use_Ferry:
					return SDK_Gameplay_Event_Player_Use_Ferry;
				case EMessageType.SDK_Gameplay_Event_Player_Tollgate_Paid:
					return SDK_Gameplay_Event_Player_Tollgate_Paid;
				case EMessageType.SDK_Gameplay_Event_Player_Fined:
					return SDK_Gameplay_Event_Player_Fined;
				case EMessageType.SDK_Gameplay_Event_Job_Delivered:
					return SDK_Gameplay_Event_Job_Delivered;
				case EMessageType.SDK_Gameplay_Event_Job_Cancelled:
					return SDK_Gameplay_Event_Job_Cancelled;
				case EMessageType.SDK_Game_Pause:
					return SDK_Game_Pause;
				case EMessageType.SDK_Game_Unpause:
					return SDK_Game_Unpause;
				case EMessageType.Truckler_Telemetry_Frame_Data:
					return Truckler_Telemetry_Frame_Data;
				case EMessageType.Truckler_Gameplay_Event:
					return Truckler_Gameplay_Event;
				case EMessageType.Truckler_Player_Info:
					return Truckler_Player_Info;
				default:
					return Unknown;
            }
        }
		public static EMessageType IntToMessageType(int val)
		{
			switch (val)
			{
				case (int)EMessageType.ServerAccept:
					return EMessageType.ServerAccept;
				case (int)EMessageType.ServerDeny:
					return EMessageType.ServerDeny;
				case (int)EMessageType.ServerShutdown:
					return EMessageType.ServerShutdown;
				case (int)EMessageType.Client_Subscribe_Events:
					return EMessageType.Client_Subscribe_Events;
				case (int)EMessageType.Client_Unsubscribe_Events:
					return EMessageType.Client_Unsubscribe_Events;
				case (int)EMessageType.Client_Request_Telemetry:
					return EMessageType.Client_Request_Telemetry;
				case (int)EMessageType.Client_Request_PlayerInfo:
					return EMessageType.Client_Request_PlayerInfo;

				case (int)EMessageType.Client_Request_HShifter_Configuration:
					return EMessageType.Client_Request_HShifter_Configuration;
				case (int)EMessageType.Client_Request_Job_Configuration:
					return EMessageType.Client_Request_Job_Configuration;
				case (int)EMessageType.Client_Request_ShifterType_Configuration:
					return EMessageType.Client_Request_ShifterType_Configuration;
				case (int)EMessageType.Client_Request_Substance_Configuration:
					return EMessageType.Client_Request_Substance_Configuration;
				case (int)EMessageType.Client_Request_Trailer_Configuration:
					return EMessageType.Client_Request_Trailer_Configuration;
				case (int)EMessageType.Client_Request_Truck_Configuration:
					return EMessageType.Client_Request_Truck_Configuration;

				case (int)EMessageType.SDK_Configuration_Truck:
					return EMessageType.SDK_Configuration_Truck;
				case (int)EMessageType.SDK_Configuration_Job:
					return EMessageType.SDK_Configuration_Job;
				case (int)EMessageType.SDK_Configuration_Trailer:
					return EMessageType.SDK_Configuration_Trailer;
				case (int)EMessageType.SDK_Configuration_Substances:
					return EMessageType.SDK_Configuration_Substances;
				case (int)EMessageType.SDK_Conifguration_ShifterType:
					return EMessageType.SDK_Conifguration_ShifterType;
				case (int)EMessageType.SDK_Configuration_HShifter:
					return EMessageType.SDK_Configuration_HShifter;
				case (int)EMessageType.SDK_Gameplay_Event_Player_Use_Train:
					return EMessageType.SDK_Gameplay_Event_Player_Use_Train;
				case (int)EMessageType.SDK_Gameplay_Event_Player_Use_Ferry:
					return EMessageType.SDK_Gameplay_Event_Player_Use_Ferry;
				case (int)EMessageType.SDK_Gameplay_Event_Player_Tollgate_Paid:
					return EMessageType.SDK_Gameplay_Event_Player_Tollgate_Paid;
				case (int)EMessageType.SDK_Gameplay_Event_Player_Fined:
					return EMessageType.SDK_Gameplay_Event_Player_Fined;
				case (int)EMessageType.SDK_Gameplay_Event_Job_Delivered:
					return EMessageType.SDK_Gameplay_Event_Job_Delivered;
				case (int)EMessageType.SDK_Gameplay_Event_Job_Cancelled:
					return EMessageType.SDK_Gameplay_Event_Job_Cancelled;
				case (int)EMessageType.SDK_Game_Pause:
					return EMessageType.SDK_Game_Pause;
				case (int)EMessageType.SDK_Game_Unpause:
					return EMessageType.SDK_Game_Unpause;
				case (int)EMessageType.Truckler_Telemetry_Frame_Data:
					return EMessageType.Truckler_Telemetry_Frame_Data;
				case (int)EMessageType.Truckler_Gameplay_Event:
					return EMessageType.Truckler_Gameplay_Event;
				case (int)EMessageType.Truckler_Player_Info:
					return EMessageType.Truckler_Player_Info;
				default:
					return EMessageType.Unknown;
			}
		}
	}
	
}
