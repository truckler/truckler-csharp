﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientRequestTelemetry : BaseMessage
    {
        internal ClientRequestTelemetry(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientRequestTelemetry Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Request_Telemetry));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientRequestTelemetry(data);
        }
    }
}
