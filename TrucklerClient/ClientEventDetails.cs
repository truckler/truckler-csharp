﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Truckler
{
    public class ClientEventDetails
    {
        public bool HasError { get; private set; }
        public Exception? Exception { get; private set; }
        public ClientEventDetails(bool hasError, Exception? e)
        {
            this.HasError = hasError;
            this.Exception = e;
        }

    }
}
