﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects
{
    public class HShifter
    {
        [JsonProperty("slotGearCount")]
        public int SlotGearCount { get; internal protected set; }
        
        [JsonProperty("slotHandlePositionCount")]
        public int SlotHandlePositionCount { get; internal protected set; }
        
        [JsonProperty("slotSelectorBitMaskCount")]
        public int SlotSelectorBitMaskCount { get; internal protected set; }
        
        [JsonProperty("slotSelectorCount")]
        public uint SlotSelectorCount { get; internal protected set; }
        
        [JsonProperty("slotGears")]
        public int[] SlotGears { get; internal protected set; }
        
        [JsonProperty("slotHandlePositions")]
        public uint[] SlotHandlePositions { get; internal protected set; }
        
        [JsonProperty("slotSelectorBitMasks")]
        public uint[] SlotSelectorBitMasks { get; internal protected set; }
        
        internal protected HShifter()
        {
            SlotGearCount = 0;
            SlotHandlePositionCount = 0;
            SlotSelectorBitMaskCount = 0;
            SlotSelectorCount = 0;
            SlotGears = new int[0];
            SlotHandlePositions = new uint[0];
            SlotSelectorBitMasks = new uint[0];
        }
    }
}
