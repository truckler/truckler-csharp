﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects
{
    
    public class JobConfiguration
    {
        [JsonProperty("cargoID")]
        public string CargoId { get; internal protected set; }

        [JsonProperty("cargo")]
        public string Cargo { get; internal protected set; }

        [JsonProperty("destinationCity")]
        public string DestinationCity { get; internal protected set; }

        [JsonProperty("destinationCityId")]
        public string DestinationCityId { get; internal protected set; }

        [JsonProperty("destinationCompany")]
        public string DestinationCompany { get; internal protected set; }

        [JsonProperty("destinationCompanyId")]
        public string DestinationCompanyId { get; internal protected set; }

        [JsonProperty("sourceCity")]
        public string SourceCity { get; internal protected set; }

        [JsonProperty("sourceCityId")]
        public string SourceCityId { get; internal protected set; }

        [JsonProperty("sourceCompany")]
        public string SourceCompany { get; internal protected set; }

        [JsonProperty("sourceCompanyId")]
        public string SourceCompanyId { get; internal protected set; }

        [JsonProperty("market")]
        public string Market { get; internal protected set; }
        
        [JsonProperty("cargoMass")]
        public float CargoMass { get; internal protected set; }

        [JsonProperty("income")]
        public uint Income { get; internal protected set; }

        [JsonProperty("deliveryTime")]
        public uint DeliveryTime { get; internal protected set; }

        [JsonProperty("cargoUnitCount")]
        public int CargoUnitCount { get; internal protected set; }

        [JsonProperty("cargoUnitMass")]
        public float CargoUnitMass { get; internal protected set; }

        [JsonProperty("isSpecialTransport")]
        public bool IsSpecialTransport { get; internal protected set; }

        [JsonProperty("plannedDistance")]
        public uint PlannedDistance { get; internal protected set; }

        [JsonProperty("onJob")]
        public bool OnJob { get; internal protected set; }
        
        [JsonProperty("cargoLoaded")]
        public bool CargoLoaded { get; internal protected set; }
        internal protected JobConfiguration()
        {
            CargoId = String.Empty;
            Cargo = String.Empty;
            DestinationCity = String.Empty;
            DestinationCityId = String.Empty;
            DestinationCompany = String.Empty;
            DestinationCompanyId = String.Empty;
            SourceCity = String.Empty;
            SourceCityId = String.Empty;
            SourceCompany = String.Empty;
            SourceCompanyId = String.Empty;
            Market = String.Empty;
            CargoMass = 0.00f;
            Income = 0;
            DeliveryTime = 0;
            CargoUnitCount = 0;
            CargoUnitMass = 0.00f;
            IsSpecialTransport = false;
            PlannedDistance = 0;
            OnJob = false;
            CargoLoaded = false;


        }
    }
}
