﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    
    public class SteamUser
    {
        [JsonProperty("DLC")]
        public DLC[] DLCs { get; internal protected set; }
        
        [JsonProperty("steamId")]
        public string SteamId { get; internal protected set; }
        [JsonProperty("username")]
        public string Username { get; internal protected set; }
        internal protected SteamUser()
        {
            DLCs = new DLC[0];
            SteamId = String.Empty;
            Username = String.Empty; 
        }
    }
}
