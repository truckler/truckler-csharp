﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    public class Damages
    {
        [JsonProperty("engine")]
        public float Engine { get; internal protected set; }
        [JsonProperty("transmission")]
        public float Transmission { get; internal protected set; }
        [JsonProperty("cabin")]
        public float Cabin { get; internal protected set; }
        [JsonProperty("chassis")]
        public float Chassis { get; internal protected set; }
        [JsonProperty("wheels")]
        public float Wheels { get; internal protected set; }
        [JsonProperty("cargo")]
        public float Cargo { get; internal protected set; }
        internal protected Damages()
        {
            Engine = 0.00f;
            Transmission = 0.00f;
            Cabin = 0.00f;
            Chassis = 0.00f;
            Wheels = 0.00f;
            Cargo = 0.00f;
        }
    }
}
