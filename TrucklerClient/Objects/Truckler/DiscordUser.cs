﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    
    public class DiscordUser
    {
        
        [JsonProperty("snowflakeId")]
        public string SnowflakeId { get; internal protected set; }
        [JsonProperty("username")]
        public string Username { get; internal protected set; }
        [JsonProperty("avatar")]
        public string Avatar { get; internal protected set; }
        [JsonProperty("discriminator")]
        public string Discriminator { get; internal protected set; }
        internal protected DiscordUser()
        {
            SnowflakeId = String.Empty;
            Avatar = String.Empty;
            Discriminator = String.Empty;
            Username = String.Empty; 
        }
    }
}
