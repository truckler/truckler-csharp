﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler.FrameData
{
    public class Game
    {
        [JsonProperty("paused")]
        public bool Paused { get; internal protected set; }
        [JsonProperty("dateTime")]
        public uint DateTime { get; internal protected set; }
        [JsonProperty("localScale")]
        public float LocalScale { get; internal protected set; }
        [JsonProperty("nextRestStop")]
        public int NextRestStop { get; internal protected set; }
        [JsonProperty("truckersMPTempId")]
        public string TruckersMPTempId { get; internal protected set; }
        [JsonProperty("truckersMPServer")]
        public string TruckersMPServer { get; internal protected set; }

        internal protected Game()
        {
            Paused = false;
            DateTime = 0;
            LocalScale = 0;
            NextRestStop = 0;
            TruckersMPServer = String.Empty;
            TruckersMPTempId = String.Empty;
        }
    }
}
