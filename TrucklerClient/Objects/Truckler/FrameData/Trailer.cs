﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler.FrameData
{
    public class Trailer
    {
        [JsonProperty("damages")]
        public Objects.Truckler.Damages Damages { get; internal protected set; }

        [JsonProperty("actualDamages")]
        public Objects.Truckler.Damages ActualDamages { get; internal protected set; }

        [JsonProperty("placement")]
        public Objects.Truckler.Placement Location { get; internal protected set; }
        
        [JsonProperty("wheels")]
        public Objects.Truckler.FrameData.Wheel[] Wheels { get; internal protected set; }
        internal protected Trailer()
        {
            Damages = new Damages();
            ActualDamages = new Damages();
            Location = new Placement();
            Wheels = new Wheel[0];
        }
    }
}
