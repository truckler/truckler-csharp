﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler.FrameData
{
    public class Job
    {
        [JsonProperty("cargoDamage")]
        public float CargoDamage { get; internal protected set; }
        [JsonProperty("isLate")]
        public bool IsLate { get; internal protected set; }
        [JsonProperty("timeRemaining")]
        public uint TimeRemaining { get; internal protected set; }
        [JsonProperty("FuelBurned")]
        public float FuelBurned { get; internal protected set; }
        [JsonProperty("fuelPurchased")]
        public float FuelPurchased { get; internal protected set; }
        [JsonProperty("distanceDriven")]
        public float DistanceDriven { get; internal protected set; }
        [JsonProperty("topSpeed")]
        public float TopSpeed { get; internal protected set; }
        internal protected Job()
        {
            CargoDamage = 0.00f;
            IsLate = false;
            TimeRemaining = 0;
            FuelBurned = 0.00f;
            FuelPurchased = 0.00f;
            DistanceDriven = 0.00f;
            TopSpeed = 0.00f;
        }
    }
}
