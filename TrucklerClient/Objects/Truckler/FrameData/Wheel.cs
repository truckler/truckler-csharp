﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler.FrameData
{
    public class Wheel
    {
        [JsonProperty("onGround")]
        public bool OnGround { get; internal protected set; }

        [JsonProperty("substance")]
        public string Substance { get; internal protected set; }

        [JsonProperty("angularVelocity")]
        public float AngularVelocity { get; internal protected set; }

        [JsonProperty("steering")]
        public float Steering { get; internal protected set; }

        [JsonProperty("rotation")]
        public float Rotation { get; internal protected set; }

        [JsonProperty("lift")]
        public float Lift { get; internal protected set; }

        [JsonProperty("liftOffset")]
        public float LiftOffset { get; internal protected set; }

        [JsonProperty("suspensionDeflection")]
        public float SuspensionDeflection { get; internal protected set; }


        internal protected Wheel()
        {
            OnGround = false;
            SuspensionDeflection = 0.00f;
            Substance = String.Empty;
            AngularVelocity = 0.00f;
            Steering = 0.00f;
            Rotation = 0.00f;
            Lift = 0.00f;
            LiftOffset = 0.00f;
        }
    }
}
