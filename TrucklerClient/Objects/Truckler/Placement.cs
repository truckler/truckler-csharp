﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    public class Placement
    {
        [JsonProperty("x")]
        public double X { get; internal protected set; }
        [JsonProperty("y")]
        public double Y { get; internal protected set; }
        [JsonProperty("z")]
        public double Z { get; internal protected set; }
        internal protected Placement()
        {
            X = 0.00f;
            Y = 0.00f;
            Z = 0.00f;
        }
    }
}
