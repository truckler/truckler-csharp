﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects
{
    public class TrailerConfiguration
    {
        [JsonProperty("wheels")]
        public WheelConfiguration[] Wheels { get; internal protected set; }

        [JsonProperty("index")]
        public int Index { get; internal protected set; }

        [JsonProperty("id")]
        public string Id { get; internal protected set; }

        [JsonProperty("brandId")]
        public string BrandId { get; internal protected set; }

        [JsonProperty("brand")]
        public string Brand { get; internal protected set; }

        [JsonProperty("name")]
        public string Name { get; internal protected set; }

        [JsonProperty("chainType")]
        public string ChainType { get; internal protected set; }

        [JsonProperty("bodyType")]
        public string BodyType { get; internal protected set; }

        [JsonProperty("licensePlate")]
        public string LicensePlate { get; internal protected set; }

        [JsonProperty("licensePlateCountry")]
        public string LicensePlateCountry { get; internal protected set; }

        [JsonProperty("licensePlateCountryId")]
        public string LicensePlateCountryId { get; internal protected set; }

        [JsonProperty("cargoAccessoryId")]
        public string CargoAccessoryId { get; internal protected set; }

        [JsonProperty("inUse")]
        public bool InUse { get; internal protected set; }

        [JsonProperty("assignedIndex")]
        public int AssignedIndex { get; internal protected set; }

        [JsonProperty("wheelCount")]
        public int WheelCount { get; internal protected set; }

        
        
        internal protected TrailerConfiguration()
        {
            Wheels = new WheelConfiguration[0];
            Index = -1;
            Id = String.Empty;
            BrandId = String.Empty;
            Brand = String.Empty;
            Name = String.Empty;
            ChainType = String.Empty;
            BodyType = String.Empty;
            LicensePlate = String.Empty;
            LicensePlateCountry = String.Empty;
            LicensePlateCountryId = String.Empty;
            CargoAccessoryId = String.Empty;
            InUse = false;
            AssignedIndex = -1;
            WheelCount = 0;
        }
    }
}
