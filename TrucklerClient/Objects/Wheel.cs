﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects
{
    public class WheelConfiguration
    {
        [JsonProperty("steerable")]
        public bool Steerable { get; internal protected set; }

        [JsonProperty("simulated")]
        public bool Simulated { get; internal protected set; }

        [JsonProperty("powered")]
        public bool Powered { get; internal protected set; }

        [JsonProperty("liftable")]
        public bool Liftable { get; internal protected set; }

        [JsonProperty("radius")]
        public float Radius { get; internal protected set; }
        internal protected WheelConfiguration()
        {
            Steerable = false;
            Simulated = false;
            Powered = false;
            Liftable = false;
            Radius = 0.00f;
        }
    }
}
