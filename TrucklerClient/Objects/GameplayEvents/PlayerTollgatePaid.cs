﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.GameplayEvents
{
    public class PlayerTollgatePaid
    {

        [JsonProperty("payAmount")]
        public long Amount { get; protected internal set; }

        internal protected PlayerTollgatePaid()
        {
            Amount = 0;
        }
    }
}
