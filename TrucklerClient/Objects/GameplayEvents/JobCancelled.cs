﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.GameplayEvents
{
    public class JobCancelled
    {
        [JsonProperty("cancelPenalty")]
        public int CancelPenalty { get; internal protected set; }
        [JsonProperty("gameTime")]
        public uint GameMinuteEnded { get; internal protected set; }
        [JsonProperty("epoch")]
        public long Epoch { get; internal protected set; }
        internal protected JobCancelled()
        {
            CancelPenalty = 0;
            GameMinuteEnded = 0;
            Epoch = 0;
        }
    }
}
