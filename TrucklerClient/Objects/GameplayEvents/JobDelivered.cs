﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.GameplayEvents
{
    public class JobDelivered
    {
        [JsonProperty("autoLoadUsed")]
        public bool AutoLoadUsed { get; protected internal set; }

        [JsonProperty("autoParkUsed")]
        public bool AutoParkUsed { get; protected internal set; }

        [JsonProperty("cargoDamage")]
        public float CargoDamage { get; protected internal set; }

        [JsonProperty("deliveryTime")]
        public uint DeliveryTime { get; protected internal set; }

        [JsonProperty("distanceDriven")]
        public float DistanceDriven { get; protected internal set; }

        [JsonProperty("earnedXp")]
        public int EarnedXP { get; protected internal set; }

        [JsonProperty("revenue")]
        public long Revenue { get; protected internal set; }

        [JsonProperty("gameTime")]
        public uint GameMinuteEnded { get; protected internal set; }

        [JsonProperty("epoch")]
        public long Epoch { get; protected internal set; }
        internal protected JobDelivered()
        {
            AutoLoadUsed = false;
            AutoParkUsed = false;
            CargoDamage = 0.00f;
            DeliveryTime = 0;
            DistanceDriven = 0.00f;
            EarnedXP = 0;
            Revenue = 0;
            GameMinuteEnded = 0;
            Epoch = 0;
        }
    }
}
